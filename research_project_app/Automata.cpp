#include "Automata.h"
#include <fstream>

void Automata::read(const std::string& file_path)
{
	std::ifstream inputFile(file_path);
	int alphabet_size, states_size, transitions_size, final_states_size;
	std::string aux_str;

	if (!inputFile)
	{
		std::cout << "Error opening file\n";
		return;
	}

	//states
	inputFile >> states_size;
	for (int index = 0; index < states_size; index++)
	{
		inputFile >> aux_str;
		m_states.push_back(aux_str);
	}

	//alphabet
	inputFile >> alphabet_size;
	for (int index = 0; index < alphabet_size; index++)
	{
		char symbol;
		inputFile >> symbol;
		m_alphabet.push_back(symbol);
	}

	//initial state
	inputFile >> aux_str;
	m_initial_state = aux_str;

	//transisions
	inputFile >> transitions_size;
	for (int index = 0; index < transitions_size; index++)
	{
		SourceSymbol aux_pair;
		char symbol;
		inputFile >> aux_str >> symbol;
		aux_pair = std::make_pair(aux_str, symbol);
		inputFile >> aux_str;
		m_transitions.emplace(aux_pair, aux_str);
	}

	//final state
	inputFile >> aux_str;
	m_final_state = aux_str;

	inputFile.close();
}

void Automata::write()
{
	std::cout << "\n\nThe states of the automata are: ";
	for (auto& index : m_states)
	{
		std::cout << index << " ";
	}

	std::cout << "\nThe alphabet: ";
	for (auto& index : m_alphabet)
	{
		std::cout << index << " ";
	}

	std::cout << "\nInitial state: " << m_initial_state;

	std::cout << "\nTransitions:\n";
	for (auto& index : m_transitions)
	{
		std::cout << index.first.first << "\t" << index.first.second << "\t" << index.second << "\n";
	}

	std::cout << "\nFinal state: " << m_final_state;
}

bool Automata::testInput(const std::string& file_path_dna)
{
	std::ifstream inputFile(file_path_dna);
	std::string current_state;
	SourceSymbol current;
	char nucleotide;

	current_state = m_initial_state;

	if (!inputFile)
	{
		std::cout << "Error opening file\n";
		return false;
	}

	while (!inputFile.eof())
	{
		inputFile >> nucleotide;
		current = setCurrent(current_state, nucleotide);

		if (m_transitions.find(current) == m_transitions.end())
		{
			inputFile.close();
			std::cout << "\nAn error occured. BLOCAJ\n";
			return false;	
		}

		if ((m_transitions.find(current)->second) == m_final_state)
		{
			inputFile.close();
			return true;
		}

		current_state = m_transitions.find(current)->second;
	}

	inputFile.close();
	std::cout << "\nDidn't find the final state.\n";
	return false;
}

bool Automata::testInputOnDataset(const std::string& file_path_dna)
{
	std::ifstream inputFile(file_path_dna);
	std::string current_state;
	SourceSymbol current;
	std::string word;
	int number_of_sequence = 0;

	if (!inputFile)
	{
		std::cout << "Error opening file\n";
		return false;
	}

	while (!inputFile.eof())
	{
		current_state = m_initial_state;
		inputFile >> word;
		//std::cout << word;
		if (word == "ORIGIN")
		{
			number_of_sequence++;
			std::string numbers = "0123456789";
			char nucleotide = 'a';
			while (nucleotide)
			{
				inputFile >> nucleotide;
				//std::cout << nucleotide;
				if (nucleotide == '/')
				{
					break;
				}
				if (numbers.find(nucleotide) == std::string::npos)
				{
					current = setCurrent(current_state, nucleotide);

					if (m_transitions.find(current) == m_transitions.end())
					{
						inputFile.close();
						std::cout <<"\nAn error occured. BLOCAJ\n";
						return false;
					}

					if ((m_transitions.find(current)->second) == m_final_state)
					{
						inputFile.close();
						std::cout << "\nSequence number " << number_of_sequence;
						return true;
					}

					current_state = m_transitions.find(current)->second;
				}
			}
		}
	}

	inputFile.close();
	std::cout << "\nCouldn't find the final state.\n";
	return false;
}

std::pair<std::string, char> Automata::setCurrent(const std::string& state, char nucleotide)
{
	SourceSymbol current;
	current = std::make_pair(state, nucleotide);
	return current;
}

bool Automata::checkTransitions()
{
	for (auto& index : m_transitions)
	{
		if (std::find(m_states.begin(), m_states.end(), index.first.first) == m_states.end())
		{
			std::cout << "\nError in transitions. The first state doesn't exist.\n";
			return false;
		}
		if (std::find(m_alphabet.begin(), m_alphabet.end(), index.first.second) == m_alphabet.end())
		{
			std::cout << "\nError in transitions. The nucleotide doesn't exist.\n";
			return false;
		}
		if (std::find(m_states.begin(), m_states.end(), index.second) == m_states.end())
		{
			std::cout << "\nError in transitions. The second state doesn't exist.\n";
			return false;
		}
	}
	return true;
}

bool Automata::checkInitialState()
{
	if (std::find(m_states.begin(), m_states.end(), m_initial_state) != m_states.end())
		return true;
	std::cout << "\nError. Couldn't find the initial state.\n";
	return false;
}

bool Automata::checkFinalState()
{
	if (std::find(m_states.begin(), m_states.end(), m_final_state) != m_states.end())
		return true;
	std::cout << "\nError. Couldn't find the final state.\n";
	return false;
}

bool Automata::checkAutomataCorrectness()
{
	if ((!checkTransitions())||(!checkInitialState())||(!checkFinalState()))
		return false;
	return true;
}
