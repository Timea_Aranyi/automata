#pragma once
#include <vector>
#include <string>
#include <map>
#include <iostream>

class Automata
{
public:
	using SourceSymbol = std::pair<std::string, char>;

	Automata() = default;
	void read(const std::string&);
	void write();
	bool testInput(const std::string&);
	bool testInputOnDataset(const std::string&);
	bool checkAutomataCorrectness();

private:
	bool checkTransitions();
	bool checkInitialState();
	bool checkFinalState();
	std::pair<std::string, char> setCurrent(const std::string&, char);

private:
	std::vector<std::string> m_states;
	std::vector<char> m_alphabet;
	std::string m_initial_state;
	std::map<SourceSymbol, std::string> m_transitions;
	std::string m_final_state;
};

