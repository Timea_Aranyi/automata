#include "Automata.h"

int main()
{
	Automata automata;
	std::string filePathAutomata = "automata_dataset.txt";
	std::string filePathDNA = "combined_GB.dat";
	int choice;

	automata.read(filePathAutomata);

	if (!automata.checkAutomataCorrectness())
	{
		std::cout << "\nThe automata is not correct.\n";
		return 0;
	}

	automata.write();

	std::cout << "\nWould you like to test a dataset, or a single DNA sequence/ genome?";
	std::cout << "\nIf you want to test a dataset, press 1, else press 2: ";
	std::cin >> choice;

	if (choice == 1)
	{
		if (automata.testInputOnDataset(filePathDNA))
		{
			std::cout << "\n\nThe DNA sequence has been accepted. The trait you were looking for exists.\n";
		}
		else
		{
			std::cout << "\nThe DNA sequence hasn't been accepted. The trait you were looking for sadly doesn't exist.\n";
		}
	}
	else
	{
		if (automata.testInput(filePathDNA))
		{
			std::cout << "\n\nThe DNA sequence has been accepted. The trait you were looking for exists.\n";
		}
		else
		{
			std::cout << "\nThe DNA sequence hasn't been accepted. The trait you were looking for sadly doesn't exist.\n";
		}
	}

	return 0;
}